import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';

// import { makeStyles } from '@material-ui/core/styles';
// import Grid from '@material-ui/core/Grid';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';
import DetailsIcon from '@material-ui/icons/Details';
import GetAppIcon from '@material-ui/icons/GetApp';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const LIST_BOOKS = gql`
  {
    search {
      items {
        id,
        title,
        series {
          volume,
          issue
        },
        writers {
          name
        },
        description,
        coverUrl,
        sources {
          id,
          sourceType,
          downloadUrl
        }
      }
    }
  }
`;

function BookList() {
  const { loading, error, data } = useQuery(LIST_BOOKS);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [sourcesList, setSourcesList] = React.useState([]);

  const handleClick = (event, sources) => {
    setSourcesList(sources);
    setAnchorEl(event.currentTarget);
  };

  const downloadBook = (event, url) => {
    setAnchorEl(null);
    window.location.href = url;
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return (
    <div>
      <GridList cellHeight={"auto"} cols={4}>
        {data.search.items.map(({ id, title, series, writers, description, coverUrl, sources }) => (
          <GridListTile key={id}>
            <img src={coverUrl} alt={title}/>
            <GridListTileBar
                title={title}
                subtitle={<span>by: {writers.map(writer => writer.name).join(", ")}</span>}
                actionIcon={
                  <ButtonGroup variant="text" color="secondary" aria-label="outlined primary button group" >
                    <IconButton onClick={() => {
                      if (series !== undefined || series.length > 1) {
                        alert(`Volume: ${series[0].volume}, Issue: ${series[0].issue}`)
                      }
                    }}>
                      <DetailsIcon />
                    </IconButton>
                    <IconButton onClick={() => alert(description)}>
                      <InfoIcon />
                    </IconButton>
                    <IconButton onClick={(event) => { handleClick(event, sources) }}>
                      <GetAppIcon />
                    </IconButton>
                  </ButtonGroup>
                }
              />
          </GridListTile>
        ))}
      </GridList>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {sourcesList.map(({id, sourceType, downloadUrl}) => (
          <MenuItem id={id} onClick={(event) => { downloadBook(event, downloadUrl) }}>{sourceType}</MenuItem>
        ))}
        
      </Menu>
    </div>
  )
}

export default BookList;
